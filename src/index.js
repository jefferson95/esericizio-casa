import { login } from "./functions/users";

const email = document.querySelector('input[name="email"]');
const password = document.querySelector('input[name="password"]');

// login(email, password, data => {
//   console.log(data);
// });

const form = document.querySelector("form");

form.addEventListener("submit", e => {
  e.preventDefault();
  login(email.value, password.value).then(data => console.log(data));
});

// esempio fatto da te molto sporco
//   const form = document.querySelector('form[name="create an account"]');
// form.addEventListener("submit", (e) => {
//   e.preventDefault();

//   fetch("https://reqres.in/api/login", {
//     body: JSON.stringify({
//       email: "eve.holt@reqres.in",
//       password: "cityslicka",
//     }),
//     headers: {
//       "Content-Type": "application/json; charset=UTF-8",
//     },
//     method: "POST",
//   })
//     .then((res) => res.json())
//     .then((data) => console.log(data));
// });
